package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		 User userss = (User) session.getAttribute("userInfo");

		if(userss == null) {
			response.sendRedirect("LoginServlet");
			return;
		}


		//DeleteServletと同じもの
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.findId(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		String id = request.getParameter("id");


		if ( !(password.equals(passwordconfirm))  || name.isEmpty() || birthDate.isEmpty()){

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(password.isEmpty() && passwordconfirm.isEmpty()) {

			UserDao userDao = new UserDao();
			userDao.Update2(name, birthDate, id);

			response.sendRedirect("UserListServlet");
			return;
		}

		UserDao userDao = new UserDao();
		userDao.Update(password, name, birthDate, id);

		response.sendRedirect("UserListServlet");


	}

}

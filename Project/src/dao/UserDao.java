package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//ログイン時
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			//MD5
			String md5 = MD5(password);

			pStmt.setString(1, loginId);
			pStmt.setString(2, md5);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}



	//新規登録・既存のIDと作成するIDで同じものがないか調べる
	public User findByLoginInfo(String loginId) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");

			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//新規登録
	public void Addinsert(String loginId, String password, String name, String birthDate ) {

		Connection conn = null;


		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "INSERT INTO user(login_id, password, name,birth_date, create_date, update_date) Values ( ?, ?, ?, ?, NOW(), NOW())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			//MD5
			String md5 = MD5(password);


			pStmt.setString(1, loginId);
			pStmt.setString(2, md5);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//アップデート
	public void Update( String password, String name, String birthDate, String id) {

		Connection conn = null;


		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = NOW() WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			//MD5
			String md5 = MD5(password);


			pStmt.setString(1, md5);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//パスワード以外
	public void Update2( String name, String birthDate, String id) {

		Connection conn = null;


		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}


	//idに紐づいているデータを取り出す
	 public User findId(String id) {
	        Connection conn = null;


		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, id);

	        ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

	        	User detail = new User();

	        	detail.setId(rs.getInt("id"));
	        	detail.setLoginId(rs.getString("login_id"));
	        	detail.setName(rs.getString("name"));
	        	detail.setBirthDate(rs.getDate("birth_date"));
	        	detail.setPassword(rs.getString("password"));
	        	detail.setCreateDate(rs.getString("create_date"));
	        	detail.setUpdateDate(rs.getString("update_date"));

		      return detail;


		}catch (SQLException e) {
    	e.printStackTrace();
    	return null;
		} finally {
		 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		 }
		}
	}

	//詳細・ユーザー１人のIDから紐づいているデータ取得

	 public List<User> findIdAll(String id) {
	        Connection conn = null;

	        List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, id);

	        ResultSet rs = pStmt.executeQuery();


	        while (rs.next()) {
	        	User detail = new User();

	        	detail.setId(rs.getInt("id"));
	        	detail.setLoginId(rs.getString("login_id"));
	        	detail.setName(rs.getString("name"));
	        	detail.setBirthDate(rs.getDate("birth_date"));
	        	detail.setPassword(rs.getString("password"));
	        	detail.setCreateDate(rs.getString("create_date"));
	        	detail.setUpdateDate(rs.getString("update_date"));

		        	userList.add(detail);

			}
		}catch (SQLException e) {
     	e.printStackTrace();
     	return null;
		} finally {
		 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		 }
		}
		return userList;
	}

	 //削除・OKボタンでユーザ情報を削除する
	 public void Delete(String id) {

			Connection conn = null;


			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// TODO ここに処理を書いていく

				String sql = "DELETE FROM user WHERE id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, id);

				pStmt.executeUpdate();



			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}

		}




	 //管理者以外を一覧表示
	 public List<User> findAll() {
	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id !=1";

			Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	        while (rs.next()) {
				int id = rs.getInt("id");
		    	String loginId = rs.getString("login_id");
		        String name = rs.getString("name");
		        Date birthDate = rs.getDate("birth_date");
		        String password = rs.getString("password");
		        String createDate = rs.getString("create_date");
		        String updateDate = rs.getString("update_date");
		        User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

		        	userList.add(user);
			}
		}catch (SQLException e) {
        	e.printStackTrace();
        	return null;
		} finally {
		 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		 }
		}
		return userList;
	}


	 //検索機能・空白の時も検索できるようにするもの
	 public List<User> findSearch(String loginIdP, String nameP, String birthDate1P, String birthDate2P) {
	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id !=1";

			if(!loginIdP.equals("")) {
				 sql += " AND login_id = '" + loginIdP + "'";
			}

			if(!nameP.equals("")) {
				 sql += " AND name LIKE '%" + nameP+ "%'";
			}

			if(!birthDate1P.equals("")) {
				 sql += " AND birth_date >= '" + birthDate1P + "'";
			}

			if(!birthDate2P.equals("")) {
				 sql += " AND birth_date <= '" + birthDate2P + "'";
			}

			Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	        while (rs.next()) {
				int id = rs.getInt("id");
		    	String loginId = rs.getString("login_id");
		        String name = rs.getString("name");
		        Date birthDate = rs.getDate("birth_date");
		        String password = rs.getString("password");
		        String createDate = rs.getString("create_date");
		        String updateDate = rs.getString("update_date");
		        User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

		        	userList.add(user);
			}
		}catch (SQLException e) {
     	e.printStackTrace();
     	return null;
		} finally {
		 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		 }
		}
		return userList;
	}


	 //MD5メソッド
	 public static String MD5(String password) {

		 String source = password;

		 Charset charset = StandardCharsets.UTF_8;

		 String algorithm = "MD5";

		 byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		 String result = DatatypeConverter.printHexBinary(bytes);
		 System.out.println(result);

		 return result;
	 }



}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html  lang="ja">
<head>
    <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <title>新規登録</title>
     <link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <nav class="navbar navbar-light bg-light">
         <span class="nav-link ml-auto mr-5">${userInfo.name}さん</span>
         <a href="LogoutServlet"><button type="submit" class="btn btn-primary mr-5"> ログアウト</button></a>
    </nav>


    <div class="container">
        <div class="sinki-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ新規登録</h1>
        </div>
        </div>
    </div>


<div class= "row justify-content-md-center">
    <div class="text-danger">${errMsg}</div>
</div>

<form class="form-horizontal" action="UserAddServlet" method="post" >

    <div class="form-group row mt-5">
                <label for="loginid" class="col-md-2 ml-auto col-form-label">ログインID</label>
                <div class="col-md-2 mr-auto">
                <input type="text" class="form-control ml-auto" name="loginId" id="loginId" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Password" class="col-md-2 ml-auto col-form-label">パスワード</label>
                <div class="col-md-2 mr-auto">
                <input type="password" class="form-control ml-auto" name="password"  id="password" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Password-confirm" class="col-md-2 ml-auto col-form-label">パスワード(確認)</label>
                <div class="col-md-2 mr-auto">
                <input type="password" class="form-control ml-auto" name="passwordconfirm"  id="passwordconfirm" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="name" class="col-md-2 ml-auto col-form-label">ユーザ名</label>
                <div class="col-md-2 mr-auto">
                <input type="text" class="form-control ml-auto" name="name"  id="name" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="birthdate" class="col-md-2 ml-auto col-form-label">生年月日</label>
                <div class="col-md-2 mr-auto">
                <input type="date" class="form-control ml-auto" name="birthDate"  id="birthDate" >
                </div>
            </div>



            <div class="row justify-content-md-center mt-5">
                <a href="userlist.html"><button type="submit" class="btn btn-primary">登録</button></a>
            </div>


                <div class="row justify-content-md-center">
                    <div class="sinki-margin2">
                        <a href="UserListServlet"><u> 戻る </u></a>
                    </div>
                </div>
</form>


</body>

</html>


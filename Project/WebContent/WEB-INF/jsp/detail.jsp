<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>ユーザ詳細</title>
    </head>

<body>
     <nav class="navbar navbar-light bg-light">
         <span class="nav-link ml-auto mr-5">${userInfo.name}さん</span>
         <a href="LogoutServlet"><button type="submit" class="btn btn-primary mr-5"> ログアウト</button></a>
    </nav>


    <div class="container">
        <div class="sinki-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ情報詳細</h1>
        </div>
        </div>
    </div>

    <div class="container">
		<c:forEach var="user" items="${userlist}" >
        <div class="form-group row mt-5">
            <label for="loginid" class="col-md-2 ml-auto col-form-label">ログインID</label>
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> ${user.loginId}
                </div>
            </div>
        </div>

        <div class="form-group row mt-3">
            <label for="name" class="col-md-2 ml-auto col-form-label">ユーザ名</label>
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> ${user.name}
                </div>
            </div>
        </div>

        <div class="form-group row justify-content-md-center mt-3">
            <label for="birthdate" class="col-md-2 ml-auto col-form-label">生年月日</label>
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> ${user.birthDate}
                </div>
            </div>
        </div>

        <div class="form-group row justify-content-md-center mt-3">
            <label for="add-date" class="col-md-2 ml-auto col-form-label">登録日時</label>
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> ${user.createDate}
                </div>
            </div>
        </div>

        <div class="form-group row justify-content-md-center mt-3">
            <label for="update-date" class="col-md-2 ml-auto col-form-label">更新日時</label>
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> ${user.updateDate}
                </div>
            </div>
        </div>

        </c:forEach>



                <div class="row justify-content-md-center">
                    <div class="sinki-margin2">
                        <a href="UserListServlet"><u> 戻る </u></a>
                    </div>
                </div>
    </div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
<head>
    <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <title>ユーザ一覧</title>
     <link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <nav class="navbar navbar-light bg-light">
         <span class="nav-link ml-auto mr-5">${userInfo.name}さん</span>
         <a href="LogoutServlet"><button type="submit" class="btn btn-primary mr-5"> ログアウト</button></a>
    </nav>


    <div class="container">
        <div class="sinki-margin">
            <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ一覧</h1>
            </div>
        </div>

        <div class="list-margin">
            <a href="UserAddServlet"><u> 新規登録 </u></a>
        </div>
     </div>


    <div class="container">
       <div class="justify-content-md-center col-md-auto">
           <form method="post" action="UserListServlet" class="form-horizontal">
            <div class="form-group row  mt-5">
                <label for="loginid" class="col-md-2 col-form-label">ログインID</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="loginIdP"  name="loginIdP" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Password" class="col-md-2 col-form-label">ユーザ名</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="nameP"  name="nameP" >
                </div>
            </div>

            <div class="form-group row  mt-3">
                <label for="birthdate" class="col-md-2 col-form-label">生年月日</label>

                <div class="row col-md-8">
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="birthDate1P"  name="birthDate1P">
                    </div>

                    <div class="col-md-2 text-center">
                        <h5>～</h5>
                    </div>

                    <div class="text-right">
                        <input type="text" class="form-control" id="birthDate2P" name="birthDate2P">
                    </div>
                </div>
            </div>


          <div class="list-margin2">
                <button type="submit" class="btn btn-primary">検索</button>
          </div>
          </form>
          </div>



        <table class="table mt-5">
            <thead class="thead-dark">
                <tr>
                <th scope="col">ログインID</th>
                <th scope="col">ユーザ名</th>
                <th scope="col">生年月日</th>
                <th scope="col"></th>
                </tr>
            </thead>
        <tbody>
        		<c:forEach var="user" items="${userList}" >
  				      <tr>
						    <td>${user.loginId}</td>
						    <td>${user.name}</td>
						    <td>${user.birthDate}</td>

				<!-- TODO 未実装；ログインボタンの表示制御を行う -->
					<c:if test="${userInfo.loginId =='admin' }">
				 		<td>
				              <a href="UserDetailServlet?id=${user.id}"><button type="button" class="btn btn-primary">詳細</button></a>
				 			  <a href="UserUpdateServlet?id=${user.id}"><button type="button" class="btn btn-success">更新</button></a>
				 			  <a href="UserDeleteServlet?id=${user.id}"><button type="button" class="btn btn-danger">削除</button></a>
						</td>
					</c:if>

					<c:if test="${userInfo.loginId !='admin' }">
				 		<td>
				              <a href="UserDetailServlet?id=${user.id}"><button type="button" class="btn btn-primary">詳細</button></a>
				            <c:if test="${userInfo.loginId==user.loginId}">
				 			  <a href="UserUpdateServlet?id=${user.id}"><button type="button" class="btn btn-success">更新</button></a>
							</c:if>
						</td>
					</c:if>

					</tr>

               </c:forEach>


        </tbody>
        </table>
    </div>

</body>
</html>



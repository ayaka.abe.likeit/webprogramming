<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>ログイン画面</title>
    </head>

<body>

 <div class="container">
     <div class="a-margin">
     <div class="row justify-content-md-center">
     <div class="col-md-auto">
        <h1>ログイン画面</h1>
     </div>
     </div>
     </div>
     	<div class="row justify-content-md-center">
     		<div class="text-danger">
     		${errMsg}
     		</div>
     	</div>

    <form class="form-signin" action="LoginServlet" method="post">

     <div class="row justify-content-md-center">
     <div class="col-md-auto mt-4">
        ログインID　<input type="text" name="loginId">
     </div>
     </div>

     <div class="row justify-content-md-center">
     <div class="col-md-auto mt-4">
        パスワード　<input type="password" name="password">
    </div>
     </div>


    <div class="row justify-content-md-center">
     <div class="col-md-auto mt-4">
         <a href="userlist.html"><button class="btn btn-primary" type="submit">ログイン</button></a>
    </div>
     </div>
     </form>
    </div>
</body>
</html>
﻿USE usermanagement;

CREATE TABLE user(
id SERIAL primary key,
login_id varchar(255) UNIQUE Not Null,
name varchar(255) Not Null,
birth_date DATE Not Null,
password varchar(255) Not Null,
create_date DATETIME Not Null,
update_date DATETIME Not Null
);

INSERT INTO user VALUES(
1,
'admin',
'管理者',
'1984-02-01',
'password',
'2019-11-12 14:45:05',
'2019-11-12 14:55:05'
);

SELECT * FROM user;

INSERT INTO user VALUES(
5,
'min',
'nanasi',
'1984-01-01',
'password',
'2019-11-12 14:45:05',
'2019-11-12 14:55:05'
);
INSERT INTO user VALUES(
3,
'ad',
'nanako',
'1995-07-01',
'password',
'2019-11-15 14:45:05',
'2019-11-20 14:55:05'
)

UPDATE user SET password = password WHERE id = 1 ;
